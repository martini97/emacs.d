;;; my-functions.el --- My functions

;;; Commentary:

;;; Code:

(defun my/org-find-file ()
  "Open Dired for (only) the Org files in and under `org-directory`."
  (interactive)
  (cd "~/org")
  (dired "*.org" "-lRF"))

(defun my/scratch-buffer ()
"Create a new scratch buffer."
(interactive)
  (let* (
      bufname
      buffer
      (n 0) )
    (catch 'done
      (while t
        (setq bufname (concat "*hello-world"
          (if (= n 0) "" (int-to-string n))
            "*"))
        (setq n (1+ n))
        (when (not (get-buffer bufname))
          (setq buffer (get-buffer-create bufname))
          (with-current-buffer buffer
            (org-mode))
          (throw 'done (display-buffer buffer t))) ))))

(provide 'my-functions)

;;; my-functions.el ends here
