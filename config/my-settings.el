;;; my-settings.el --- Settings
;;; Commentary:
;;; Code:

(defalias 'yes-or-no-p 'y-or-n-p)
(setq-default show-paren-delay 0)
(setq inhibit-startup-message t
      visible-bell nil
      ring-bell-function 'ignore
      line-number-mode t
      column-number-mode t)
(setq-default show-trailing-whitespace t)
(mouse-wheel-mode -1)

;; identation
(defvar custom-indent-width "Custom identation width.")
(setq custom-indent-width 2)

(setq-default indent-tabs-mode nil)
(setq-default standard-indent custom-indent-width)
(setq-default evil-shift-width custom-indent-width)
(setq-default tab-width custom-indent-width)

(setq backward-delete-char-untabify-method 'hungry)

(setq backup-directory-alist `(("." . ,(expand-file-name "~/.emacs.d/backups/"))))

(provide 'my-settings)
;;; my-settings.el ends here
