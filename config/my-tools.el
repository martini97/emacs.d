;;; my-tools.el --- Tolls
;;; Commentary:
;;; Code:

(use-package avy)

(use-package magit)

(use-package smartparens
  :config
  (add-hook 'prog-mode-hook #'smartparens-mode))

(use-package counsel
  :config
  (ivy-mode 1)
  (setq counsel-find-file-ignore-regexp "\(?:\‘[#.]\)\|\(?:[#~]\’\)")
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-re-builders-alist
	'((t . ivy--regex-fuzzy))))

(use-package company
  :defer 2
  :diminish
  :custom
  (company-begin-commands '(self-insert-command))
  (company-idle-delay .1)
  (company-minimum-prefix-length 2)
  (company-show-numbers t)
  (global-company-mode t))

(use-package yasnippet
  :config
  (yas-global-mode 1))

(use-package yasnippet-snippets
  :after yasnippet)

(use-package multi-term
  :config
  (setq multi-term-program "/bin/zsh")
  (add-hook 'term-mode-hook
	    (lambda ()
	      (setq show-trailing-whitespace nil)
              (add-to-list 'term-bind-key-alist '("M-[" . multi-term-prev))
	      (add-to-list 'term-bind-key-alist '("M-]" . multi-term-next))
	      (define-key term-mode-map (kbd "C-j") 'term-char-mode)
	      (define-key term-raw-map (kbd "C-j") 'term-line-mode)
	      (setq-default multi-term-dedicated-select t)
	      (setq multi-term-dedicated-close-back-to-open-buffer-p t))))

(use-package flycheck
  :init (global-flycheck-mode)
  :config (add-hook 'after-init-hook #'global-flycheck-mode))

(provide 'my-tools)

;;; my-tools.el ends here
