;;; my-theme.el -- my emacs theme
;;; Commentary:
;;; Code:
(use-package darkokai-theme
  :config
  (setq darkokai-mode-line-padding 1)
  (load-theme 'darkokai t))

(when window-system
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (tooltip-mode -1))

(show-paren-mode 1)

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(use-package spaceline
  :init
  (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)
  :config
  (spaceline-spacemacs-theme))

(provide 'my-theme)
;;; my-theme.el ends here
