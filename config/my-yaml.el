(use-package yaml-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.y(a)?ml\\'" . yaml-mode)))
