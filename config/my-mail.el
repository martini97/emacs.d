(setq gnus-select-method
      '(nnimap "gmail"
	       (nnimap-address "imap.gmail.com")
	       (nnimap-server-port "imaps")
	       (nnimap-stream ssl)))

(setq smtpmail-smtp-server "smtp.gmail.com"
      smtpmail-smtp-service 587
      gnus-ignored-newsgroups "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]"
      send-mail-function 'smtpmail-send-it
      message-send-mail-function 'smtpmail-send-it)

(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu4e")
(require 'mu4e)
(require 'org-mu4e)

(setq mail-user-agent 'mu4e-user-agent
  mu4e-drafts-folder "/[Gmail].Drafts"
  mu4e-sent-folder "/[Gmail].Sent Mail"
  mu4e-trash-folder "/[Gmail].Trash"
  mu4e-get-mail-command "offlineimap"
  mu4e-sent-messages-behavior 'delete
  mu4e-compose-dont-reply-to-self t
  mu4e-maildir-shortcuts '(("/INBOX"               . ?i)
      	                   ("/[Gmail].Sent Mail"   . ?s)
                           ("/[Gmail].Trash"       . ?t)
                           ("/[Gmail].All Mail"    . ?a))
  mu4e-user-mail-address-list '("alessandro.martini@bravi.com.br")
  mu4e-html2text-command "html2text -utf8 -width 80"
  mu4e-compose-signature (concat
                          "Alessandro Martini\n"
                          "Software Engineer @ Bravi Solutions\n"
                          "www.bravi.com.br")
  mu4e-update-interval 300
  mu4e-index-cleanup nil
  mu4e-index-lazy-check t
  message-kill-buffer-on-exit t)

(use-package mu4e-alert
  :ensure t
  :config
  (add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display))


(require 'gnus-dired)
(defun gnus-dired-mail-buffers ()
  "Return a list of active message buffers."
  (let (buffers)
    (save-current-buffer
      (dolist (buffer (buffer-list t))
        (set-buffer buffer)
        (when (and (derived-mode-p 'message-mode)
                (null message-sent-message-via))
          (push (buffer-name buffer) buffers))))
    (nreverse buffers)))

(setq gnus-dired-mail-mode 'mu4e-user-agent)
(add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)
