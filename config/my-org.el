;;; my-org.el -- my org config
;;; Commentary:
;;; Code:
(use-package org)
;; (use-package org-plus-contrib
;;   :pin org)

(setq org-todo-keywords
      (quote ((sequence "TODO(t)" "NEXT(n)" "CURRENT(c)" "|" "DONE(d)")
	      (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(x@/!)" "EMAIL(e)" "MEETING(m)"))))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
	      ("NEXT" :foreground "blue" :weight bold)
	      ("CURRENT" :foreground "yellow" :weight bold)
	      ("DONE" :foreground "forest green" :weight bold)
	      ("WAITING" :foreground "orange" :weight bold)
	      ("HOLD" :foreground "magenta" :weight bold)
	      ("CANCELLED" :foreground "forest green" :weight bold)
	      ("MEETING" :foreground "forest green" :weight bold)
	      ("EMAIL" :foreground "forest green" :weight bold))))

(add-to-list 'auto-mode-alist '("\\.\\(org\\|org_archive\\|txt\\)$" . org-mode))
(setq org-treat-S-cursor-todo-selection-as-state-change nil)
(setq org-log-done t)
(setq org-agenda-files (list "~/org/work.org" "~/org/personal.org"))
(setq org-highest-priority 65)
(setq org-lowest-priority 75)
(setq org-default-priority 67)
(setq org-use-property-inheritance '("PRIORITY"))
(setq org-log-into-drawer "LOGBOOK")
(setq org-use-fast-todo-selection t)
(setq org-fontify-whole-heading-line t)
(setq org-fontify-done-headline t)
(setq org-startup-indented t)
(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)
(defun org-summary-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise."
  (let (org-log-done org-log-states)   ; turn off logging
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))
(add-hook 'org-after-todo-statistics-hook 'org-summary-todo)

(defconst org-refile-file
  (concat org-directory "/refile.org")
  "File to temporarily write notes, that will be organized later.")
(setq org-default-notes-file org-refile-file)
(setq org-refile-targets (quote ((nil :maxlevel . 9)
				 (org-agenda-files :maxlevel . 9))))

 (setq org-capture-templates
	(quote (("t" "todo" entry (file org-refile-file)
		 "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
		("n" "note" entry (file org-refile-file)
		 "* %? :note:\n%U\n%a\n" :clock-in t :clock-resume t)
		("j" "journal" entry (file+datetree "~/org/journal.org")
		 "* %?\n%U\n" :clock-in t :clock-resume t)
		("m" "meeting" entry (file org-refile-file)
		 "* MEETING %? :meeting:\n%t" :clock-in t :clock-resume t)
	        ("s" "daily (stand up)" entry (file org-refile-file)
           	 "* DAILY %t :daily:\n%?" :clock-in t :clock-resume t)
		("c" "contacts" entry (file "~/org/contacts.org")
		 "* %(org-contacts-template-name)
:PROPERTIES:
:BIRTHDAY: %^{yyyy-mm-dd}
:EMAIL: %(org-contacts-template-email)
:PHONE: %^(PHONE)
:NOTE: %^{NOTE}
:END:")
		("b" "book" entry (file "~/org/reading.org")
		 "* %^{TITLE}
:PROPERTIES:
:AUTHOR: %^{AUTHOR}
:LINK: %A
:ADDED: %U
:END:"))))

(setq org-ellipsis "↴")
(use-package org-bullets
  :config
  (setq org-bullets-bullet-list
        '("►" "•" "▸"))
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(org-babel-do-load-languages 'org-babel-load-languages
                             '((shell . t)))

(setq-default org-clock-report-include-clocking-task t)

(provide 'my-org)
;;; my-org.el ends here
