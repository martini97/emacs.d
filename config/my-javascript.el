;;; my-javascript.el -- my js config
;;; Commentary:
;;; Code:
(use-package js2-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
  (add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
  (setq-default js2-basic-indent 2
	js2-basic-offset 2
	js2-auto-indent-p t
	js2-cleanup-whitespace t
	js2-enter-indents-newline t
	js2-indent-on-enter-key t))

(use-package js2-refactor
  :config
  (add-hook 'js2-mode-hook #'js2-refactor-mode)
  (js2r-add-keybindings-with-prefix "C-c C-r")
  (define-key js2-mode-map (kbd "C-k") #'js2r-kill)
  (define-key js-mode-map (kbd "M-.") nil)
  (add-hook 'js2-mode-hook
	  (lambda ()
	    (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t))))

(use-package rjsx-mode)

(use-package xref-js2)

(use-package tern
  :after (js2-mode)
  :init
  (add-hook 'js2-mode-hook (lambda () (tern-mode t)))
  :config
  (define-key tern-mode-keymap (kbd "M-.") nil)
  (define-key tern-mode-keymap (kbd "M-,") nil)
  (setq tern-command (append tern-command '("--no-port-file"))))

(use-package company-tern
  :after (company tern)
  :config
  (add-to-list 'company-backends 'company-tern)
  (add-hook 'js2-mode-hook (lambda ()
                           (tern-mode)
                           (company-mode))))

(use-package tide
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
         (before-save . tide-format-before-save)))

(use-package js-doc
  :config
  (setq js-doc-mail-address "alessandro.martini@bravi.com.br"
        js-doc-author (format "Alessandro Martini <%s>" js-doc-mail-address))
  (add-hook 'js2-mode-hook
            #'(lambda ()
                (define-key js2-mode-map "\C-ci" 'js-doc-insert-function-doc)
                (define-key js2-mode-map "@" 'js-doc-insert-tag))))

(provide 'my-javascript)
;;; my-javascript.el ends here
