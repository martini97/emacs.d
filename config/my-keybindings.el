;;; my-keybindings.el -- my keybindings
;;; Commentary:
;;; Code:

(use-package which-key
  :config
  (which-key-mode))

(use-package undo-tree
  :config
  (global-undo-tree-mode))

(use-package goto-chg)

(use-package evil
  :init
  (setq evil-want-integration nil)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(which-key-add-key-based-replacements
  "SPC ," "local"
  "SPC k" "goto"
  "SPC g" "git"
  "SPC o" "org"
  "SPC t" "term"
  "SPC f" "file"
  "SPC b" "buffer")

(use-package general
  :config
  (defconst my-leader "SPC")
  (defconst my-local-leader "SPC ,")
  (defconst my-goto-leader "SPC k")
  (defconst my-magit-leader "SPC g")
  (defconst my-org-leader "SPC o")
  (defconst my-term-leader "SPC t")
  (defconst my-file-leader "SPC f")
  (defconst my-buffer-leader "SPC b")
  (general-create-definer my-leader-def :prefix my-leader)
  (general-create-definer my-local-leader-def :prefix my-local-leader)
  (general-create-definer my-goto-leader-def :prefix my-goto-leader)
  (general-create-definer my-magit-leader-def :prefix my-magit-leader)
  (general-create-definer my-org-leader-def :prefix my-org-leader)
  (general-create-definer my-term-leader-def :prefix my-term-leader)
  (general-create-definer my-file-leader-def :prefix my-file-leader)
  (general-create-definer my-buffer-leader-def :prefix my-buffer-leader)
  (general-define-key
   :keymaps 'normal
   "C-s" #'swiper)
  (my-leader-def
    :keymaps 'normal
    "x" '(counsel-M-x :which-key "command")
    "s" '(counsel-ag :which-key "search")
    "m" '(mu4e :which-key "mail")
    "Q" '(save-buffers-kill-terminal :which-key "exit"))
  (my-local-leader-def
    :states 'normal
    :keymaps 'org-mode-map
    "i" '(org-clock-in :which-key "clock in")
    "I" '(org-clock-in-last :which-key "clock in last")
    "o" '(org-clock-out :which-key "clock out"))
  (my-goto-leader-def
    :keymaps 'normal
    "c" #'avy-goto-char
    "l" #'avy-goto-line
    "w" #'avy-goto-word-1)
  (my-org-leader-def
   :keymaps 'normal
   "a" '(org-agenda :which-key "agenda")
   "c" '(org-capture :which-key "capture")
   "l" '(org-store-link :which-key "link")
   "b" '(org-switchb :which-key "buffer")
   "g" '(org-clock-goto :which-key "goto current")
   "f" '(my/org-find-file :which-key "file"))
  (my-magit-leader-def
   :keymaps 'normal
   "b" '(magit-blame :which-key "blame")
   "s" '(magit-status :which-key "status"))
  (my-term-leader-def
   :keymaps 'normal
   "SPC" '(multi-term-dedicated-toggle :which-key "dedicated term")
   "n" '(multi-term-next :which-key "term next")
   "p" '(multi-term-prev :which-key "term prev")
   "t" '(multi-term :which-key "term"))
  (my-file-leader-def
   :keymaps 'normal
   "f" '(counsel-find-file :which-key "find")
   "d" '(dired :which-key "dired")
   "s" '(save-buffer :which-key "save"))
  (my-buffer-leader-def
   :keymaps 'normal
   "f" '(ivy-switch-buffer :which-key "list")
   "s" '(my/scratch-buffer :which-key "scratch")
   "e" '(eval-buffer :which-key "eval")
   "b" '(ibuffer :which-key "ibuffer")))

(provide 'my-keybindings)
;;; my-keybindings.el ends here
