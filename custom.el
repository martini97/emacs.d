(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-contacts-files (quote ("~/org/contacts.org")) t)
 '(package-selected-packages
   (quote
    (all-the-icons-ivy company-anaconda anaconda-mode company-box js-doc typescript-mode tide rjsx-mode htmlize neotree indium latex-preview-pane ein flymake-json elpy org-plus-contrib org org-contacts spaceline yaml-mode company-tern tern xref-js2 js2-refactor js2-mode flycheck ibuffer-vc multi-term smartparens yasnippet-snippets yasnippet company org-bullets magit counsel evil-collection avy mu4e-alert general helm which-key evil goto-chg undo-tree darkokai-theme use-package)))
 '(send-mail-function (quote mailclient-send-it)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
