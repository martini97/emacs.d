;;; init.el -- emacs config
;;; Commentary:
;;; Code:
(add-to-list 'load-path "~/.emacs.d/config")
(load-library "my-packages")
(load-library "my-theme")
(load-library "my-functions")
(load-library "my-mail")
(load-library "my-org")
(load-library "my-tools")
(load-library "my-buffers")
(load-library "my-settings")
(load-library "my-javascript")
(load-library "my-python")
(load-library "my-yaml")
(load-library "my-keybindings")
(provide 'init)
;;; init.el ends here
